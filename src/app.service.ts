import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
  getHelloWorld(): string {
    return 'Hello World!';
  }
  getHello(): string {
    return '<html><body><h1>Hello Buu</html></body></h1>';
  }
  convert(celsius: number) {
    return { celsius: celsius, fahrenheit: (celsius * 9.0) / 5 + 32 };
  }
}
